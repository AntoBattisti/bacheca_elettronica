#include <sys/signal.h> 
#include <stdlib.h> 
#include <stdio.h>
#include <unistd.h> 
#include <sys/socket.h> 
#include <netdb.h> 
#include <string.h> 
#include <inttypes.h> 

#define exit_error(x){printf("Errore: "x"\n"); exit(EXIT_FAILURE);}
#define error_conn(x){printf("Errore:"x"\n"); if(close(ds_socket)==-1){ printf("Errore:chiusura socket\n"); exit(EXIT_FAILURE);} return -1;}


#define DIM_USER 24 
#define DIM_OGG 50
#define DIM_TESTO 256


#define IP_LENGTH 25
#define DIM_RISPOSTA 25



typedef struct{
	int   codice; 
	char* mittente;
	char* oggetto;
	char* testo;
}messaggio;




int ds_socket;
int connessione=1; 
struct sockaddr_in server; 
char mitt_utente[DIM_USER]; 



void gestione_chiusura(){
	puts("\nChiusura client in corso...");
	close(ds_socket);
    	exit(EXIT_SUCCESS);
  }
void gestione_connessione(){
    	close(ds_socket);
	connessione=1;
}
void gestione_errori(){
    close(ds_socket);
    exit(EXIT_FAILURE);
}


int prendi_intero(){ 
	int n=-1;
	while(1){
	 	char* p;
	  	char* buff=NULL;
	  	size_t num=0;
	  	if(getline(&buff,&num,stdin)>0){ 
	 		n=strtoumax(buff,&p,10); 
			    if(*p=='\n'||*p=='\0') break; 
	     	}else{
		    printf("Input non cosiderato corretto:riprovare");
	     	}

	}	
	return n;
}



int prendi_stringa(char* ip,int dim){ 
	int num;
	while(1){
		char* line=NULL;
		size_t n=0;
		num=getline(&line,&n,stdin);
		if(num<dim && num>0){
			strcpy(ip,line);
			num--;
			ip[num]='\0'; 
			break;
		}else{
			printf("Hai inserito una stringa troppo grande: Inserire una stringa di massimo %d caratteri",dim);
		}
	}
	return 0;
}



int login(){ 
	puts("Login utente");
	ds_socket=socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
	if(ds_socket==-1) exit_error(" creazione socket");
	char utente[DIM_USER];
	char password[DIM_USER];
	char ris[DIM_RISPOSTA];
	int key=4; 
	printf("Inserisci il nome utente [ max %d caratteri ]=",DIM_USER);
	prendi_stringa(utente,DIM_USER);
	printf("Inserisci la password [ max %d caratteri ]=",DIM_USER);
	prendi_stringa(password,DIM_USER);
	if(connect(ds_socket,(struct sockaddr*)&server,sizeof(server))==-1){
		close(ds_socket);
		exit_error("connect");
	}
	
	if(connessione!=1) 
		error_conn("connessione canale")
	if(send(ds_socket,&key,sizeof(int),0)==-1){
		printf("Errore send\n");
		if(close(ds_socket)==-1){
			exit_error("chiusura socket");
		}
		return -1;
	}
	if(connessione!=1) 
		error_conn("connessione canale")
	
	if(send(ds_socket,utente,DIM_USER,0)==-1){
		printf("Errore send\n");
		if(close(ds_socket)==-1){
			exit_error("chiusura socket");
		}
		return -1;
	}
	if(connessione!=1) 
		error_conn("connessione canale")
	
	if(send(ds_socket,password,DIM_USER,0)==-1){
		printf("Errore send\n");
		if(close(ds_socket)==-1){
			exit_error("chiusura socket");
		}
		return -1;
	}
	if(connessione!=1) 
		error_conn("connessione canale")
	
	if(recv(ds_socket,ris,DIM_RISPOSTA,0)==-1){
		printf("Errore recv\n");
		if(close(ds_socket)==-1){
			exit_error("chiusura socket");
		}
		return -1;
	}
	if(strcmp(ris,"ok")==0){
		strcpy(mitt_utente,utente);
		return 1;
	}
	printf("Errore:nome utente o password non corrette\n");
	return -1;	
}



int registrazione(){ 
	puts("Registrazione utente");
	ds_socket=socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
	if(ds_socket==-1) 
		exit_error(" creazione socket");
	char utente[DIM_USER];
	char password[DIM_USER];
	char ris[DIM_RISPOSTA];
	int key=5; 
	printf("Inserisci il nome utente [ massimo %d caratteri ]=",DIM_USER);
	prendi_stringa(utente,DIM_USER);
	printf("Inserisci la password [ massimo %d caratteri ]=",DIM_USER);
	prendi_stringa(password,DIM_USER);
	if(connect(ds_socket,(struct sockaddr*)&server,sizeof(server))==-1){
		close(ds_socket);
		exit_error("connect");
	}
	
	if(connessione!=1) 
		error_conn("connessione canale")
	if(send(ds_socket,&key,sizeof(int),0)==-1){
		printf("Errore send\n");
		if(close(ds_socket)==-1){
			exit_error("chiusura socket");
		}
		return -1;
	}
	if(connessione!=1) 
		error_conn("connessione canale")
	
	if(send(ds_socket,utente,DIM_USER,0)==-1){
		printf("Errore send\n");
		if(close(ds_socket)==-1){
			exit_error("chiusura socket");
		}
		return -1;
	}
	if(connessione!=1) 
		error_conn("connessione canale")
	
	if(send(ds_socket,password,DIM_USER,0)==-1){
		printf("Errore send\n");
		if(close(ds_socket)==-1){
			exit_error("chiusura socket");
		}
		return -1;
	}
	if(connessione!=1) error_conn("connessione canale")
	
	if(recv(ds_socket,ris,DIM_RISPOSTA,0)==-1){
		printf("Errore recv\n");
		if(close(ds_socket)==-1){
			exit_error("chiusura socket");
		}
		return -1;
	}
	if(strcmp(ris,"ok")==0){
		strcpy(mitt_utente,utente);
		return 0;
	}
	printf("Errore:registrazione non andata a buon fine");
	return -1;		
}



int leggi_bacheca(){ 
	puts("Lettura Bacheca");
	ds_socket=socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
	if(ds_socket==-1) 
		exit_error(" creazione socket");
	int scelta=1; 
	if(connect(ds_socket,(struct sockaddr*)&server,sizeof(server))==-1){
		close(ds_socket);
		exit_error("connect");
	}
	if(connessione!=1) 
		error_conn("connessione canale")
	if(send(ds_socket,&scelta,sizeof(int),0)==-1){
		printf("Errore send\n");
		if(close(ds_socket)==-1){
			exit_error("chiusura socket");
		}
		return -1;
	}
	puts("+++++ Bacheca +++++");
	int i=0;
	int num;
	if(connessione!=1) 
		error_conn("connessione canale")
	if(recv(ds_socket,&num,sizeof(int),MSG_WAITALL)==-1){ 
		printf("Errore recv\n");
		if(close(ds_socket)==-1){
			exit_error("chiusura socket");
		}
		return -1;
	}
	while(i<num){
	int codice;
	char mittente[DIM_USER];
	char oggetto[DIM_OGG];
	char testo[DIM_TESTO];
	if(connessione!=1) 
		error_conn("connessione canale")
	if(recv(ds_socket,&codice,sizeof(int),MSG_WAITALL)==-1){
		printf("Errore ricezione dati\n");
		if(close(ds_socket)==-1){
			exit_error("chiusura socket");
		}
		return -1;
	}
	if(connessione!=1) 
		error_conn("connessione canale")
	if(recv(ds_socket,mittente,DIM_USER,MSG_WAITALL)==-1){
		printf("Errore ricezione dati\n");
		if(close(ds_socket)==-1){
			exit_error("chiusura socket");
		}
		return -1;
	}
	if(connessione!=1) 
		error_conn("connessione canale")
	if(recv(ds_socket,oggetto,DIM_OGG,MSG_WAITALL)==-1){
		printf("Errore ricezione dati\n");
		if(close(ds_socket)==-1){
			exit_error("chiusura socket");
		}
		return -1;
	}
	if(connessione!=1) 
		error_conn("connessione canale")
	if(recv(ds_socket,testo,DIM_TESTO,MSG_WAITALL)==-1){
		printf("Errore ricezione dati\n");
		if(close(ds_socket)==-1){
			exit_error("chiusura socket");
		}
		return -1;
	}
	puts("Messaggio: ");
	printf("mittente=%s\noggetto=%s\ntesto=%s\n",mittente,oggetto,testo);
	i++;
	}
	puts("-------------------------------------------------");
	if(close(ds_socket)==-1){
			exit_error("chiusura socket");
		}
		puts("");
    return 0;
}




int scrivi_in_bacheca(){ 
	puts("Scrittura in bacheca");
	ds_socket=socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
	if(ds_socket==-1) exit_error(" creazione socket");
	int scelta=2; 
	messaggio* m=malloc(sizeof(messaggio));
	char mit[DIM_USER];
	char ogg[DIM_OGG];
	char tes[DIM_TESTO];
	strcpy(mit,mitt_utente);
	int i=0;
	while(mit[i]!='\0') 
		i++;
	while(i<DIM_USER) 
		mit[i++]='\0'; 
	m->mittente=mit;
	puts("Inserire l'oggetto del messaggio che si vuole scrivere:");
	prendi_stringa(ogg,DIM_OGG);
	m->oggetto=ogg;
	puts("Inserire il testo del messaggio che si vuole scrivere:");
	prendi_stringa(tes,DIM_TESTO);
	m->testo=tes;
	printf("Messaggio da inviare:\n mittente=%s\n oggetto=%s \n testo=%s \n",m->mittente,m->oggetto,m->testo);
	if(connect(ds_socket,(struct sockaddr*)&server,sizeof(server))==-1){
		close(ds_socket);
		exit_error("connect");
	}
	if(connessione!=1) 
		error_conn("connessione canale")
	if(send(ds_socket,&scelta,sizeof(int),0)==-1){
		printf("Errore send\n");
		if(close(ds_socket)==-1){
			exit_error("chiusura socket");
		}
		return -1;
	}
	char ris2[DIM_RISPOSTA];
	if(connessione!=1) 
		error_conn("connessione canale")
	if(recv(ds_socket,ris2,DIM_RISPOSTA,0)==-1){
		printf("Errore recv\n");
		if(close(ds_socket)==-1){
			exit_error("chiusura socket");
		}
		return -1;
	}
	if(strcmp(ris2,"ok")!=0){
		puts("Errore:Memoria piena");
		return 0;
	}
	
	if(connessione!=1) 
		error_conn("connessione canale")
	if(send(ds_socket,m->mittente,DIM_USER,0)==-1){
		printf("Errore send\n");
		if(close(ds_socket)==-1){
			exit_error("chiusura socket");
		}
		return -1;
	}
   	
    	if(connessione!=1) 
		error_conn("connessione canale")
	if(send(ds_socket,m->oggetto,DIM_OGG,0)==-1){
		printf("Errore send\n");
		if(close(ds_socket)==-1){
			exit_error("chiusura socket");
		}
		return -1;
	}
  		
    	if(connessione!=1) 
		error_conn("connessione canale")
	if(send(ds_socket,m->testo,DIM_TESTO,0)==-1){
		printf("Errore send\n");
		if(close(ds_socket)==-1){
			exit_error("chiusura socket");
		}
		return -1;
	}
	free(m);
	char risp[DIM_RISPOSTA];
	if(connessione!=1) 
		error_conn("connessione canale")
	if(recv(ds_socket,risp,DIM_RISPOSTA,0)==-1){
		printf("Errore recv\n");
		if(close(ds_socket)==-1){
			exit_error("chiusura socket");
		}
		return -1;
	}
	if(close(ds_socket)==-1){
			exit_error("chiusura socket");
		}
	if(strcmp(risp,"ok")==0){
		puts("Scrittura in bacheca completata");
		return 0;	
	}
	puts("Errore: riprovare");
	return -1;
}



int elimina_dalla_bacheca(){ 
	puts("Eliminazione messaggio");
	ds_socket=socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
	if(ds_socket==-1) exit_error(" creazione socket");
	int scelta=3; 
	if(connect(ds_socket,(struct sockaddr*)&server,sizeof(server))==-1){
		close(ds_socket);
		exit_error("connect");
	}
	if(send(ds_socket,&scelta,sizeof(int),0)==-1){
		printf("Errore send\n");
		if(close(ds_socket)==-1){
			exit_error("chiusura socket");
		}
		return -1;
	}
	if(connessione!=1) 
		error_conn("connessione canale")
	if(send(ds_socket,mitt_utente,DIM_USER,0)==-1){
		printf("Errore send\n");
		if(close(ds_socket)==-1){
			exit_error("chiusura socket");
		}
		return -1;
	}
	puts("Messaggi precedenti scritti in bacheca");
	int i=0,j=0;
	int num;
	if(connessione!=1) 
		error_conn("connessione canale")
	if(recv(ds_socket,&num,sizeof(int),MSG_WAITALL)==-1){
		printf("Errore recv\n");
		if(close(ds_socket)==-1){
			exit_error("chiusura socket");
		}
		return -1;
	}
	if(num==0){
		puts("Nessun messaggio precedente scritto in bacheca");
	}
	int lista[num];
	char processa[DIM_RISPOSTA];
	while(i<num){
	int codice=0;
	char oggetto[DIM_OGG];
	char testo[DIM_TESTO];
	if(connessione!=1) 
		error_conn("connessione canale")
	if(recv(ds_socket,processa,DIM_RISPOSTA,MSG_WAITALL)==-1){
		printf("Errore recv\n");
		if(close(ds_socket)==-1){
			exit_error("chiusura socket");
		}
		return -1;
	}
	if(strcmp(processa,"trovato")==0){ 
		if(recv(ds_socket,&codice,sizeof(int),MSG_WAITALL)==-1){
			printf("Errore recv\n");
				if(close(ds_socket)==-1){
					exit_error("chiusura socket");
				}
			return -1;
		}
	if(connessione!=1) 
		error_conn("connessione canale")
	if(recv(ds_socket,oggetto,DIM_OGG,MSG_WAITALL)==-1){
		printf("Errore recv\n");
		if(close(ds_socket)==-1){
			exit_error("chiusura socket");
		}
		return -1;
	}
	if(connessione!=1) 
		error_conn("connessione canale")
	if(recv(ds_socket,testo,DIM_TESTO,MSG_WAITALL)==-1){
		printf("Errore recv\n");
		if(close(ds_socket)==-1){
			exit_error("chiusura socket");
		}
		return -1;
	}
	printf("mesaggio codice=%d\nmittente=%s\noggetto=%s\ntesto=%s\n",codice,mitt_utente,oggetto,testo);
	lista[j]=codice;
	j++;
	}
	i++;
	}
	if(close(ds_socket)==-1){
			exit_error("chiusura socket");
		}
   	int a;
   	puts("Codice dei messaggi");
   	for(a=0;a<j;a++){
	   printf("%d ",lista[a]);
   	}
   	puts("");
   	int msg_scelto;
   	while(1){
   	puts("Inserire il codice del messaggio da cancellare:");
   	msg_scelto=prendi_intero();
   	if(msg_scelto<0||msg_scelto>num){
	   puts("Codice inserito non valido, riprovare");
	   continue;
   	}
   	break;
	}
	scelta=33; 
   	ds_socket=socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
	if(ds_socket==-1) 
		exit_error(" creazione socket");
	if(connect(ds_socket,(struct sockaddr*)&server,sizeof(server))==-1){
		close(ds_socket);
		exit_error("connect");
	}
	if(send(ds_socket,&scelta,sizeof(int),0)==-1){
		printf("Errore send\n");
		if(close(ds_socket)==-1){
			exit_error("chiusura socket");
		}
		return -1;
	}
	if(connessione!=1) 
		error_conn("connessione canale")
	if(send(ds_socket,&msg_scelto,sizeof(int),0)==-1){
		printf("Errore send\n");
		if(close(ds_socket)==-1){
			exit_error("chiusura socket");
		}
		return -1;
	}
	char risposta[DIM_RISPOSTA];
	if(connessione!=1) 
		error_conn("connessione canale")
	if(recv(ds_socket,risposta,DIM_RISPOSTA,0)==-1){
		printf("Errore recv\n");
		if(close(ds_socket)==-1){
			exit_error("chiusura socket");
		}
		return -1;
	}
	if(close(ds_socket)==-1){
			exit_error("chiusura socket");
		}
	if(strcmp(risposta,"ok")==0){
		puts("Eliminazione messaggio completata");
		return 0;
	}
	puts("Errore, riprovare");
	return -1;
}



int main(int argc,char** argv){
	

	
	int port;
	char ip_server[IP_LENGTH];
	struct hostent *hp;



	puts("\n");
	puts("    ******    BACHECA ELETTRONICA REMOTA    ******     ");

	
	
	struct sigaction sa;
	sa.sa_flags=0;
	sigfillset(&sa.sa_mask); 
	sa.sa_handler=gestione_chiusura;
	if(sigaction(SIGINT,&sa,NULL)==-1)  
		exit_error("sigaction");
	if(sigaction(SIGQUIT,&sa,NULL)==-1) 
		exit_error("sigaction"); 
    	if(sigaction(SIGHUP,&sa,NULL)==-1)  
		exit_error("sigaction");
    	if(sigaction(SIGTERM,&sa,NULL)==-1) 
		exit_error("sigaction"); 
    
    
    	sa.sa_handler=gestione_connessione;
    	if(sigaction(SIGPIPE,&sa,NULL)==-1) 
		exit_error("sigaction"); 
    
    
    	sa.sa_handler=gestione_errori;
    	if(sigaction(SIGILL,&sa,NULL)==-1) 
		exit_error("sigaction"); 
    	if(sigaction(SIGSEGV,&sa,NULL)==-1) 
		exit_error("sigaction");

	
	
	while(1){
	printf("Inserisci l'indirizzo del server: ");
	prendi_stringa(ip_server,IP_LENGTH);
	printf("Inserisci il numero di porta del server:");
	port=prendi_intero();
	server.sin_family=AF_INET;
	server.sin_port=htons(port);
	hp=gethostbyname(ip_server);
	if(hp==NULL){
		puts("Errore Host: Riprovare");
		continue;
	}
	bcopy(hp->h_addr,&server.sin_addr,hp->h_length);
	break;
	}

	
	
	puts("Scegli tra le due opzioni possibili:");
	while(1){
	puts("1.Login");
	puts("2.Registrazione");
	int scelta;
	scelta=prendi_intero();
	if(scelta==1){
		
		if(login()==-1) puts("Log In utente fallito, riprovare");
		else break;
	}else{
		
		if(registrazione()==-1) puts("Registrazione fallita, riprovare");
		else break;
	 }
 	}

	
	
 	while(1){
	puts("");	 
 	puts("+++ Benvenuto nella bacheca elettronica remota +++");
 	puts("");
	int scelta;
 	puts("[1] Leggi dalla bacheca elettronica");
 	puts("[2] Scrivi nella bacheca elettronica");
 	puts("[3] Elimina un tuo messaggio dalla bacheca elettronica");
 	puts("[4] Esci dalla bacheca elettronica");
	scelta=prendi_intero();
 	switch(scelta){
		case 1:
		leggi_bacheca();
		break;
	  	case 2:
	 	scrivi_in_bacheca();
	 	break;
	  	case 3:
	 	elimina_dalla_bacheca();
	 	break;
	  	case 4:
	 	puts("Arrivederci");
	 	exit(EXIT_SUCCESS);
	 	break;
	 	default:
	 	puts("scelta non valida, riprovare");
	}
	}

	exit(0);
}