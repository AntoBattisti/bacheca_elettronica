#include <stdio.h>  
#include <string.h>
#include <stdlib.h>   
#include <unistd.h> 
#include <fcntl.h>  
#include <sys/socket.h> 
#include <inttypes.h>
#include <netinet/in.h> 
#include <time.h>  
#include <sys/signal.h> 
#include <arpa/inet.h>


#define exit_error(x){printf("Errore: "x"\n"); exit(EXIT_FAILURE);}
#define error_conn(x){printf("Errore:"x"\n"); return -1;}


#define DIM_USER 24 
#define DIM_OGG 50
#define DIM_TESTO 256

#define DIM_MSG_TOT sizeof(int)+DIM_USER+DIM_OGG+DIM_TESTO 
#define DIM_RISPOSTA 25
#define MAX_DIM 1000


typedef struct{
	int codice;
	char mittente[DIM_USER];
	char oggetto[DIM_OGG];
	char testo[DIM_TESTO];
	void* next; 
}messaggio;



int max_msg; 
int next_code; 
messaggio* init; 
messaggio* final; 
int client_sock; 
int file_users; 
int connessione=1; 
int ds_socket; 
int tipo_exit=EXIT_SUCCESS; 


void start_bacheca(){ 
	puts("Inizializzazione bacheca...");
	int file_bacheca=open("bacheca",O_RDWR|O_CREAT|O_APPEND, 0660);
	if(file_bacheca==-1) 
		exit_error("creazione file bacheca");
	int inizio=0;
	int fine=lseek(file_bacheca,0,2);
	lseek(file_bacheca,0,0);
	int i=0; 
	while(inizio<fine){
		messaggio* prov=malloc(sizeof(messaggio));
		if(read(file_bacheca,prov,DIM_MSG_TOT)==-1)
			exit_error("lettura da file bacheca")
		prov->next=NULL;
		if(inizio==0){
			final=prov;
			init=final;
		}else{
			final->next=(messaggio*)prov;
			final=prov;
		}
		inizio=inizio+DIM_MSG_TOT;
		i++;
	}
	max_msg=i;
	if(max_msg>=1)
		next_code=final->codice+1;
	else next_code=0;
	printf("Numero messaggi:%d\n",max_msg);
	if(close(file_bacheca)==-1) 
		exit_error("chiusura file bacheca");
	puts("Inizializzazione bacheca completata!");
}

void salva_bacheca(){ 
	int file_bacheca=open("bacheca",O_RDWR|O_CREAT|O_TRUNC,0660);
	if(file_bacheca==-1) 
		exit_error("apertura file bacheca")
	int i=0;
	messaggio* msg=init;
	messaggio* p;
	while(i<max_msg){
		if(write(file_bacheca,msg,DIM_MSG_TOT)==-1) 					
			exit_error("scrittura su file")
		p=msg;
    		if(msg->next!=NULL){
			msg=(messaggio*)msg->next;
			free(p);
			i++;
		} else break;
	}
	if(close(file_bacheca)==-1) 
		exit_error("chiusura file bacheca");
}


void gestione_chiusura(){
	printf("\nChiusura server in corso... \n ");
	salva_bacheca();
	close(ds_socket);
	close(client_sock);
	close(file_users);
	printf("\nChiusura server completata\n");
	exit(tipo_exit);
  }

void gestione_connessione(){
	puts("Errore connessione");
	connessione=0;
}

void gestione_errori(){
	puts("Azione non consentita");
    	tipo_exit=EXIT_FAILURE;
    	raise(SIGINT); 
}

void gestione_timeout(){
	puts("Timeout connessione");
	connessione=0;
}


int nuovo_utente(){ 
	puts("Registrazione");
	char reg[DIM_USER*2];
	if(connessione!=1) 
		error_conn("connessione canale")
	
	if(recv(client_sock,&reg[0],DIM_USER,MSG_WAITALL)==-1){
		 puts("Errore ricezione utente");
		 return -1;
	}
	if(connessione!=1) 
		error_conn("connessione canale")
	
	if(recv(client_sock,&reg[DIM_USER],DIM_USER,MSG_WAITALL)==-1){
		 puts("Errore ricezione password");
		 return -1;
	} 
	int controllo=0;
	int inizio=0;
	int fine=lseek(file_users,0,2); 
	lseek(file_users,0,0);
	while(inizio<fine){
		char ver_utente[DIM_USER];
		if(read(file_users,ver_utente,DIM_USER)==-1){
			 puts("Errore lettura utente");
			 return -1;
	 	}
		if(strcmp(ver_utente,&reg[0])==0) 
			controllo=1;
		lseek(file_users,DIM_USER,1); 
		inizio=inizio+2*DIM_USER; 
	}
	if(controllo==0){
		if(write(file_users,reg,DIM_USER*2)==-1){
			 puts("Errore scrittura nuovo utente");
			 return -1;
		}
		if(connessione!=1) 
			error_conn("connessione canale")
		if(send(client_sock,"ok",DIM_RISPOSTA,0)==-1){
			 puts("Errore send risposta");
			 return -1;
		}
        	printf("registrazione avvenuta con successo:\n utente=%s\n password=%s\n",&reg[0],&reg[DIM_USER]);
		return 0;
	}
	if(controllo==1){  
		if(connessione!=1) 
			error_conn("connessione canale")
		if(send(client_sock,"user",DIM_RISPOSTA,0)==-1){
			 puts("Errore send risposta");
			 return -1;
		}
		puts("Utente gia registrato");
		return -1;
	}
	if(connessione!=1) error_conn("connessione canale")
		if(send(client_sock,"errore",DIM_RISPOSTA,0)==-1){
			 puts("Errore send risposta");
			 return -1;
		}
	puts("errore imprevisto");
	return -1;
}

int login(){ 
	puts("Login");
	char utente[DIM_USER];
	char pass[DIM_USER];
	int valida; 
	if(connessione!=1) 
			error_conn("connessione canale")
	if(recv(client_sock,utente,DIM_USER,MSG_WAITALL)==-1){
		 puts("Errore recv utente");
			 return -1;
	}
	if(connessione!=1) 
			error_conn("connessione canale")
	if(recv(client_sock,pass,DIM_USER,MSG_WAITALL)==-1){
		 puts("Errore recv password");
			 return -1;
	}
	
	int inizio=0;
	int fine=lseek(file_users,0,2);
	lseek(file_users,0,0);
	while(inizio<fine){
		char ver_utente[DIM_USER];
		if(read(file_users,ver_utente,DIM_USER)==-1){
			 puts("Errore read utente");
			 return -1;
		}
		if(strcmp(ver_utente,utente)==0){
			
			char ver_pass[DIM_USER];
		if(read(file_users,ver_pass,DIM_USER)==-1){
			 puts("Errore read utente");
			 return -1;
		}
		if(strcmp(ver_pass,pass)==0){
			puts("Login effettuato con successo");
			valida=0;
		}else{
			puts("Password inserita sbagliata");
			valida=2;
		}
		}else{
			lseek(file_users,DIM_USER,1);
		}
		inizio=inizio+2*DIM_USER;
 	}
	if(valida!=0&&valida!=2){
    		puts("Utente non trovato");
    		valida=1;
   	}
    
	if(valida==0){
		
		if(connessione!=1) 
			error_conn("connessione canale")
		if(send(client_sock,"ok",DIM_RISPOSTA,0)==-1){
			 puts("Errore send risposta");
			 return -1;
		 }
		 return 0;
	 }
	 if(valida==1){
		
		if(connessione!=1) 
			error_conn("connessione canale")
		if(send(client_sock,"user",DIM_RISPOSTA,0)==-1){
			 puts("Errore send risposta");
			 return -1;
		 }
		 return 1;
	 }
	 if(valida==2){
		
		if(connessione!=1) 
			error_conn("connessione canale")
		if(send(client_sock,"password",DIM_RISPOSTA,0)==-1){
			 puts("Errore send risposta");
			 return -1;
		 }
		 return 2;
	 }
		
	if(connessione!=1) 
		error_conn("connessione canale")
	if(send(client_sock,"errore",DIM_RISPOSTA,0)==-1){
		 puts("Errore send risposta");
		 return -1;
		 }
    	 puts("errore imprevisto");		 
		 return -1;
}


int leggi_bacheca(){ 
	puts("Lettura bacheca");
	int i=0;
	if(connessione!=1) 
		error_conn("connessione canale")
	if(send(client_sock,&max_msg,sizeof(int),0)==-1){
		 puts("Errore send ");
		 return -1;
	}
	if(max_msg<1) 
		return 0;
	messaggio* msg=init;
	while(i<max_msg){
		
		if(connessione!=1) 
		error_conn("connessione canale")
		if(send(client_sock,&msg->codice,sizeof(int),0)==-1){
			 puts("Errore send ");
			 return -1;
		}
		
		if(connessione!=1) 
		error_conn("connessione canale")
		if(send(client_sock,msg->mittente,DIM_USER,0)==-1){
			 puts("Errore send ");
			 return -1;
		}
		
		if(connessione!=1) 
		error_conn("connessione canale")
		if(send(client_sock,msg->oggetto,DIM_OGG,0)==-1){
			 puts("Errore send ");
			 return -1;
		}
		
		if(connessione!=1) 
		error_conn("connessione canale")
		if(send(client_sock,msg->testo,DIM_TESTO,0)==-1){
			 puts("Errore send ");
			 return -1;
		}
		msg=(messaggio*)msg->next; 
		i++;
	}
	return 0;
}


int scrivi_in_bacheca(){ 
	puts("Scrittura in bacheca");
	if(max_msg>=MAX_DIM){
		if(connessione!=1) 
		error_conn("connessione canale")
		if(send(client_sock,"bacheca piena",DIM_RISPOSTA,0)==-1){
			 puts("Errore send ");
			 return -1;
		 }
		puts("memoria piena");
		return 1;
	}
	if(connessione!=1) 
		error_conn("connessione canale")
	if(send(client_sock,"ok",DIM_RISPOSTA,0)==-1){
			 puts("Errore send ");
			 return -1;
		}
	messaggio* msg=malloc(sizeof(messaggio));

	if(connessione!=1) 
		error_conn("connessione canale")
	if(recv(client_sock,msg->mittente,DIM_USER,MSG_WAITALL)==-1){
		free(msg);
		puts("Errore recv messaggio ");
		return -1;
	}
	
	if(connessione!=1) 
		error_conn("connessione canale")
	if(recv(client_sock,msg->oggetto,DIM_OGG,MSG_WAITALL)==-1){
		free(msg);
		puts("Errore recv messaggio ");
		return -1;
	}
	
	if(connessione!=1) 
		error_conn("connessione canale")
	if(recv(client_sock,msg->testo,DIM_TESTO,MSG_WAITALL)==-1){
		free(msg);
		puts("Errore recv messaggio ");
		return -1;
	}
	
	msg->codice=next_code;
	next_code++;
	msg->next=NULL;
	if(max_msg==0){
		init=msg;
	}else{
		final->next=(messaggio*)msg;
	}
	final=msg;
	max_msg++;
	printf("Messaggio scritto in bacheca:\n code=%d\n  mittente=%s\n oggetto=%s\n testo=%s\n",final->codice ,final->mittente,final->oggetto,final->testo);
	if(connessione!=1) 
		error_conn("connessione canale")
	if(send(client_sock,"ok",DIM_RISPOSTA,0)==-1){
		 puts("errore send risposta ");
			 return -1;
	}
	return 0;
}


int invio_msg(){
	puts("Invio proprio messaggi");
	char mitt_utente[DIM_USER]; 
	if(connessione!=1) 
		error_conn("connessione canale")
	if(recv(client_sock,mitt_utente,DIM_USER,MSG_WAITALL)==-1){
		puts("Errore recv ");
		return -1;
	}
	printf("utente=%s\n",mitt_utente);
	if(connessione!=1) 
		error_conn("connessione canale")
	if(send(client_sock,&max_msg,sizeof(int),0)==-1){
		puts("Errore send  ");
		return -1;
	}
	int i=0;
	messaggio* msg=init;
	while(i<max_msg){
		if(strcmp(msg->mittente,mitt_utente)==0){
			if(connessione!=1) 
				error_conn("connessione canale")
			if(send(client_sock,"trovato",DIM_RISPOSTA,0)==-1){
				puts("Errore send ");
			     	return -1;
			}
			if(connessione!=1) 
					error_conn("connessione canale")
			if(send(client_sock,&msg->codice,sizeof(int),0)==-1){
				puts("Errore send ");
			     	return -1;
			}
			if(connessione!=1) 
				error_conn("connessione canale")
			if(send(client_sock,msg->oggetto,DIM_OGG,0)==-1){
				puts("Errore send ");
			     	return -1;
			}
			if(connessione!=1) 
				error_conn("connessione canale")
			if(send(client_sock,msg->testo,DIM_TESTO,0)==-1){
				puts("Errore send ");
			     	return -1;
			}
		}else{
			if(connessione!=1) 
				error_conn("connessione canale")
			if(send(client_sock,"salta",DIM_RISPOSTA,0)==-1){
				puts("Errore send ");
			     	return -1;
			}
		}		
		i++;
		msg=(messaggio*)msg->next;
	}
	return 0;
}

int cancella_msg(){ 
	puts("Cancellazione messaggio scelto");
	int scelto;
	if(connessione!=1) 
		error_conn("connessione canale")
	if(recv(client_sock,&scelto,sizeof(int),MSG_WAITALL)==-1){
		puts("Errore recv");
		return -1;
	}
	printf("Eliminazione messaggio codice: %d \n",scelto);	
	int i=0;
	messaggio* prec; 
	messaggio* msg=init;
	while(i<max_msg){
		if(msg->codice==scelto) 
			break;
		if(msg->next==NULL){
			if(connessione!=1) 
				error_conn("connessione canale")
			if(send(client_sock,"messaggio gia eliminato",DIM_RISPOSTA,0)==-1){
				 puts("Errore send ");
			    	 return -1;
				}		
			return 0;
		}
		prec=msg;
		msg=(messaggio*)msg->next;
		i++;
	}
	if(max_msg>1){
		if(i==0){ 
			init=(messaggio*)msg->next;
		}else{
			if(i==max_msg-1){ 
				prec->next=NULL;
				final=prec;
			}else{
				prec->next=(messaggio*)msg->next; 
			}
		}
	}
	max_msg--;
	free(msg);
	if(connessione!=1) 
		error_conn("connessione canale")
	if(send(client_sock,"ok",DIM_RISPOSTA,0)==-1){
		puts("Errore send ");
		return -1;
	}
	puts("Cancellazione messaggio completata");
	return 0;	
}


int main(int argc,char **argv){
	

	
    	struct sigaction sa;
	sa.sa_flags=0;
	sigfillset(&sa.sa_mask); 
	sa.sa_handler=gestione_chiusura;
	if(sigaction(SIGINT,&sa,NULL)==-1) 
		exit_error("sigaction");
	if(sigaction(SIGQUIT,&sa,NULL)==-1) 
		exit_error("sigaction"); 
    	if(sigaction(SIGHUP,&sa,NULL)==-1) 
		exit_error("sigaction");
   	if(sigaction(SIGTERM,&sa,NULL)==-1) 
		exit_error("sigaction");
    
    
    	sa.sa_handler=gestione_connessione;
    	if(sigaction(SIGPIPE,&sa,NULL)==-1) 
		exit_error("sigaction"); 
     
    
   	sa.sa_handler=gestione_errori;
    	if(sigaction(SIGILL,&sa,NULL)==-1)  
		exit_error("sigaction"); 
    	if(sigaction(SIGSEGV,&sa,NULL)==-1) 
		exit_error("sigaction");
    

    	sa.sa_handler=gestione_timeout;
    	if(sigaction(SIGALRM,&sa,NULL)==-1) 
		exit_error("sigaction");
     


	puts("");
	printf("*** Server Bacheca elettronica ***\n" );
	
	struct sockaddr_in server; 
	struct sockaddr client;   
	socklen_t size_client=sizeof(client);
	int port;
	printf("Inserire il numero di porta:");
	puts("");
	while(1){
	  char* p;
	  char* buff=NULL;
	  size_t n=0;
	  if(getline(&buff,&n,stdin)>0){
		port=strtoumax(buff,&p,10);
		if(buff[0]!='\n'&&(*p=='\n'||*p=='\0')) 
			break;
	  }else{
		puts("Input non cosiderato corretto:riprovare");}
    	}
	printf("Inizializzazione socket...\n");
	ds_socket=socket(AF_INET,SOCK_STREAM,0);
	if(ds_socket==-1) 
		exit_error("creazione socket")
	server.sin_family=AF_INET;
	server.sin_port=htons(port);
	server.sin_addr.s_addr=INADDR_ANY;
	if(bind(ds_socket,(struct sockaddr*)&server,sizeof(server))==-1){
		close(ds_socket);
		exit_error("binding")
	}
	listen(ds_socket,500); 
	puts("Inizializzazione strutture dati...");
	file_users=open("users",O_RDWR|O_CREAT|O_APPEND,0660); 
	if(file_users==-1){
		close(ds_socket);
		exit_error("apertura file utente")
	}
	start_bacheca();
   	while(1){
		puts("Attendere client... [CTRL+C per chiudere ]");
		while((client_sock=accept(ds_socket,&client,&size_client))==-1);
		alarm(5);
		puts("Client connesso");
		int scelta;
		if(recv(client_sock,&scelta,sizeof(int),MSG_WAITALL)==-1){
			puts("Errore ricezione scelta client");
			alarm(0);
			if(close(client_sock)==-1){
				tipo_exit=EXIT_FAILURE;
				raise(SIGINT);
			}
		continue;
		}
		printf("Scelta client:");
		switch(scelta){
			case 1:
			leggi_bacheca();
			break;
			case 2:
			scrivi_in_bacheca();
			break;
			case 3: 
			invio_msg();
			break;
			case 33: 
			cancella_msg();
			break;
			case 4: 
			login();
			break;
			case 5:
			nuovo_utente();
			break;
			default:
			printf("Scelta non valida\n");
		}
		if(client_sock!=-1) 
			if(close(client_sock)==-1) 
				exit_error("chiusura socket client");
    			alarm(0);
			connessione=1;

	}
}
